name := "endpoint-builder"

version := "1.0"

scalaVersion := "2.11.1"

//val SPRAY_VER = "1.3.1"

resolvers ++= Seq("Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/",
              "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
              "Spray" at "http://repo.spray.io",
  "Mvn" at "http://mvnrepository.com/artifact")


libraryDependencies ++= Seq( "com.typesafe.akka" %% "akka-actor" % "2.3.4",
  "com.typesafe.play" %% "play-json" % "2.3.0",
  "io.spray" %% "spray-can" % "1.3.1",
  "io.spray" %% "spray-client" % "1.3.1"
)
    