/**
 * Created by kevin.harper on 5/21/15.
 */

import java.io.{File, FileWriter, BufferedWriter}

import play.api.libs.json.{JsArray, JsString, Json, JsValue}
import spray.httpx.encoding.{Gzip, Encoder}
import sun.misc.BASE64Decoder
import scala.io.{Codec, Source}
import akka.actor.ActorSystem
import spray.client.pipelining._
import spray.http.{HttpRequest, ContentType, HttpEntity, HttpResponse}
import spray.httpx.RequestBuilding
import scala.concurrent.{Future, duration, Await}
import scala.concurrent.duration._
import scala.io.Source



object EndpointBuilder {
  val usage = "Usages:  EndpointBuilder.main domain outputfile \n Example: EndpointBuilder.main http://ep4.nbc2.bottlerocketservices.com/live output.txt"
  def main(args: Array[String]): Unit ={
    if(args.length != 2){
      print(usage)
    } else {
      buildEndpoints(args(0), args(1))
    }

  }
  def buildEndpoints(domain: String, outputFile: String): Unit = {
 //   val domain = "http://ep4.nbc2.bottlerocketservices.com/live"
    val versions = List("2","3")
    val devices = List("iPhone","iPhoneRetina","iPad","iPadRetina","Tablet","TabletHD","Phone","PhoneHD")
    val functions = List("config", "tape")
    //build tape and config endpoints
    val coreUrls = for {
      version  <- versions
      function <- functions
      device   <- devices
    } yield {
      domain + "/" + version + "/nbce/" + function + "/" + device
    }
    val assetUrls = buildAssetEndpointsFromUrl(domain, versions, devices)
    writeToFile(coreUrls, assetUrls, outputFile)
  }

  def buildAssetEndpointsFromFile(domain: String, versions: List[String], devices: List[String]) ={
    val tapeContents = Source.fromFile("src/main/resources/tape_nbce.json")
    val assetList = parseTapeForAssetList(Some(tapeContents.mkString)).get
    val assetUrls = for {
      device <- devices
      version  <- versions
      assetRaw <- assetList
    } yield {
        val asset = assetRaw.as[String]
        s"$domain/$version/nbce/asset/$device/$asset"
      }
    assetUrls
  }

  def buildAssetEndpointsFromUrl(domain: String, versions: List[String], devices: List[String]) ={
    // The iPhone tape has some additional endpoints
    val tapeEndpoint = s"$domain/3/nbce/tape/iPhone"
    val tapeContents = getTapeFromUrl(tapeEndpoint)
    val assetList = parseTapeForAssetList(tapeContents).get
    val assetUrls = for {
      device <- devices
      version  <- versions
      assetRaw <- assetList
    } yield {
        val asset = assetRaw.as[String]
        s"$domain/$version/nbce/asset/$device/$asset"
      }
    assetUrls
  }

  def parseTapeForAssetList(tape: Option[String]) = {
    val json = Json.parse(tape.get)
    val assets = json \\ "assetID"
    Some(assets)
  }

  def getTapeFromUrl(tapeUrl: String): Option[String] ={
    implicit val system = ActorSystem("EndpointBuilder")
    import system.dispatcher
    val pipeline: HttpRequest => Future[HttpResponse] = (addHeader("Accept-Encoding", "gzip") ~> sendReceive ~> decode(Gzip))
    val pipelineResponse = pipeline(Get(tapeUrl))
    val httpResponse = Await.result(pipelineResponse, 10 seconds)
    if(httpResponse.message.status.isFailure){
      None
    } else {
      system.shutdown()
      Some(httpResponse.message.entity.asString)
    }

  }

  def writeToFile(list1: List[String], list2: List[String], outputFilename: String): Unit ={
    val outputFile = new File(outputFilename)
    val writer = new BufferedWriter(new FileWriter(outputFile))
    val combinedList = list1.map(_ + "\n") ::: list2.map(_ + "\n")
    combinedList.map(item => writer.write(item))
    writer.close()
  }

}
